#![allow(dead_code)]

use byteorder::{LittleEndian, ReadBytesExt};
use std::io::{Cursor, Error, Read, Seek, SeekFrom};

pub(crate) struct BootRecordVolume {
    pub(crate) boot: u8,
    pub(crate) iso_ident: String,
    pub(crate) version: u8,
    pub(crate) torito_spec: String,
    pub(crate) boot_p: usize,
}

impl TryFrom<&[u8]> for BootRecordVolume {
    type Error = Error;

    /// unpack boot_record_volume:
    /// boot: u8
    /// iso_ident: [u8; 5] as &str
    /// version: u8
    /// torito_spec: [u8; 32] as &str
    /// unused: [u8; 32] as &str
    /// boot_p: u32 in le
    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        let mut cursor = Cursor::new(value);
        let boot = cursor.read_u8()?;
        let iso_ident = {
            let mut buf = [0; 5];
            cursor.read_exact(&mut buf)?;
            String::from_utf8_lossy(&buf).trim_end().to_owned()
        };

        let version = cursor.read_u8()?;
        let torito_spec = {
            let mut buf = [0; 32];
            cursor.read_exact(&mut buf)?;
            String::from_utf8_lossy(&buf).trim_end().to_owned()
        };
        cursor.seek(SeekFrom::Current(32))?;
        let boot_p = cursor.read_u32::<LittleEndian>()? as usize;
        Ok(Self {
            boot,
            iso_ident,
            version,
            torito_spec,
            boot_p,
        })
    }
}

impl BootRecordVolume {
    pub(crate) fn is_cd(&self) -> bool {
        self.iso_ident == "CD001"
    }

    pub(crate) fn is_torito_spec(&self) -> bool {
        self.torito_spec.contains("EL TORITO SPECIFICATION")
    }
}

pub(crate) struct BootingCatalog {
    pub(crate) header: u8,
    pub(crate) platform: u8,
    pub(crate) manufact: String,
    pub(crate) five: u8,
    pub(crate) aa: u8,
}

impl TryFrom<&[u8]> for BootingCatalog {
    type Error = Error;

    /// unpack validate_entry:
    /// header: u8
    /// platform: u8
    /// unused: u16 in le
    /// manufact: [u8; 24] as &str
    /// unused: u16 in le
    /// five: u8
    /// aa: u8
    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        let mut cursor = Cursor::new(value);
        let header = cursor.read_u8()?;
        let platform = cursor.read_u8()?;
        cursor.seek(SeekFrom::Current(2))?;
        let manufact = {
            let mut buf = [0; 24];
            cursor.read_exact(&mut buf)?;
            String::from_utf8_lossy(&buf).trim_end().to_owned()
        };
        cursor.seek(SeekFrom::Current(2))?;
        let five = cursor.read_u8()?;
        let aa = cursor.read_u8()?;

        Ok(Self {
            header,
            platform,
            manufact,
            five,
            aa,
        })
    }
}

impl BootingCatalog {
    pub(crate) fn get_platform(&self) -> &str {
        match self.platform {
            0 => "x86",
            1 => "PowerPc",
            2 => "Mac",
            _ => "unknown",
        }
    }

    pub(crate) fn is_valid(&self) -> bool {
        self.header == 1u8 && self.five == 0x55 && self.aa == 0xaa
    }
}

pub(crate) struct InitialEntry {
    pub(crate) boot: u8,
    pub(crate) media: u8,
    pub(crate) lead_segment: u16,
    pub(crate) system_type: u8,
    pub(crate) s_count: u16,
    pub(crate) img_start: usize,
}

impl TryFrom<&[u8]> for InitialEntry {
    type Error = Error;

    /// unpack initial_entry:
    /// boot: u8
    /// media: u8
    /// lead_segment: u16 in le
    /// system_type: u8
    /// unused: u8
    /// s_count: u16 in le
    /// img_start: u32 as le
    /// unused: u8
    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        let mut cursor = Cursor::new(value);
        let boot = cursor.read_u8()?;
        let media = cursor.read_u8()?;
        let lead_segment = cursor.read_u16::<LittleEndian>()?;
        let system_type = cursor.read_u8()?;
        cursor.seek(SeekFrom::Current(1))?;
        let s_count = cursor.read_u16::<LittleEndian>()?;
        let img_start = cursor.read_u32::<LittleEndian>()? as usize;

        Ok(Self {
            boot,
            media,
            lead_segment,
            system_type,
            s_count,
            img_start,
        })
    }
}

impl InitialEntry {
    pub(crate) fn is_boot(&self) -> bool {
        self.boot == 0x88
    }

    pub(crate) fn get_boot_media_type(&self) -> &str {
        match self.media {
            0 => "no emulation",
            1 => "1.2meg floppy",
            2 => "1.44meg floppy",
            3 => "2.88meg floppy",
            4 => "harddisk",
            _ => "",
        }
    }
    pub(crate) fn get_boot_media_type_count(&self) -> Option<usize> {
        match self.media {
            0 => Some(0),
            1 => Some(1200 * 1024 / crate::ToritoConf::V_SECTOR_SIZE),
            2 => Some(1440 * 1024 / crate::ToritoConf::V_SECTOR_SIZE),
            3 => Some(2880 * 1024 / crate::ToritoConf::V_SECTOR_SIZE),
            4 => None,
            _ => unreachable!(),
        }
    }
}

pub(crate) struct Partition {
    first_sector: usize,
    partition_size: usize,
}

impl TryFrom<&[u8]> for Partition {
    type Error = Error;

    /// unpack partition1:
    /// unused: [u8; 8] as &str
    /// first_sector: u64 as le
    /// partition_size: u64 as le
    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        let mut cursor = Cursor::new(value);
        cursor.seek(SeekFrom::Current(8))?;

        let first_sector = cursor.read_u32::<LittleEndian>()? as usize;
        let partition_size = cursor.read_u32::<LittleEndian>()? as usize;
        Ok(Self {
            first_sector,
            partition_size,
        })
    }
}

impl Partition {
    pub(crate) fn count(&self) -> usize {
        self.first_sector + self.partition_size
    }
}
