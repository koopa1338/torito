use std::{
    fs::{File, OpenOptions},
    io::{self, stdout, Read, Seek, SeekFrom::Start, Write},
    path::PathBuf,
};

use clap::Parser;

mod sector_types;
use sector_types::{BootRecordVolume, BootingCatalog, InitialEntry, Partition};

#[derive(Debug, Clone, Parser)]
#[clap(author, version, about, long_about = None)]
struct ToritoConf {
    #[clap(long, short)]
    /// src file of bootable CD image
    src: PathBuf,
    #[clap(long, short)]
    /// Write extracted data to file <OUT>, defaults to STDOUT.
    out: Option<PathBuf>,
}

impl ToritoConf {
    const V_SECTOR_SIZE: usize = 512;
    const SECTOR_SIZE: usize = 2048;

    fn get_sector(num: usize, count: usize, mut file: &File) -> io::Result<Vec<u8>> {
        let position = num * Self::SECTOR_SIZE;
        file.seek(Start(position as u64))?;

        let buffer_size = count * Self::V_SECTOR_SIZE;
        let mut buffer: Vec<u8> = vec![0; buffer_size];
        file.read_exact(&mut buffer)?;

        Ok(buffer)
    }

    fn write_output(&self, data: &[u8]) -> io::Result<()> {
        if let Some(out) = &self.out {
            let mut file = OpenOptions::new()
                .read(true)
                .write(true)
                .create(true)
                .open(out)?;

            file.write_all(data)?;
            println!();
            println!(
                r#"Image has been written to file "{}"."#,
                out.file_name()
                    .expect("Path should have a valid file component.")
                    .to_string_lossy()
            );
        } else {
            stdout().write_all(data)?;
            println!("Image has been written to stdout ....");
        }

        Ok(())
    }
}

fn main() -> io::Result<()> {
    let run_config = ToritoConf::parse();

    let image = {
        let file = File::open(&run_config.src)?;
        let boot_record_volume = ToritoConf::get_sector(17, 1, &file)?;
        let brv: BootRecordVolume = boot_record_volume.as_slice().try_into()?;

        println!("Booting catalog starts at sector: {}", brv.boot_p);

        if !brv.is_cd() || !brv.is_torito_spec() {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                "This data image does not seem to be a bootable CD-image",
            ));
        }

        let booting_catalog = ToritoConf::get_sector(brv.boot_p, 1, &file)?;
        let bc: BootingCatalog = booting_catalog[..32].try_into()?;

        if !bc.is_valid() {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                "Invalid Validation Entry on image",
            ));
        }

        println!("Manufacturer of CD: {}", bc.manufact);
        println!("Image architecture: {}", bc.get_platform());

        let ie: InitialEntry = booting_catalog[32..64].try_into()?;

        if !ie.is_boot() {
            return Err(io::Error::new(
                io::ErrorKind::InvalidData,
                "Boot indicator in Initial/Default-Entry is not 0x88. CD is not bootable.",
            ));
        }

        print!("Boot media type is: {}", ie.get_boot_media_type());
        let count = if let Some(c) = ie.get_boot_media_type_count() {
            c
        } else {
            let mbr = ToritoConf::get_sector(ie.img_start, 1, &file)?;
            let partition: Partition = mbr[446..462].try_into()?;
            partition.count()
        };

        let cnt = if count == 0 {
            ie.s_count as usize
        } else {
            count
        };

        println!();
        println!(
            "El Torito image starts at sector {i_start} and has {cnt} sector(s) of {sector_size} Bytes",
            i_start = ie.img_start,
            sector_size = ToritoConf::V_SECTOR_SIZE
        );

        ToritoConf::get_sector(ie.img_start, cnt, &file)?
    };

    run_config.write_output(&image)
}
