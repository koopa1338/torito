## Torito-rs

A Rust implementation of the `geteltorito` tool, originally created by Rainer Krienke.

### Usage

```sh
Usage: torito-rs [OPTIONS] --src <SRC>

Options:
  -s, --src <SRC>  src file of bootable CD image
  -o, --out <OUT>  Write extracted data to file <OUT>, defaults to STDOUT
  -h, --help       Print help
  -V, --version    Print version
```

## Acknowledgements

Original geteltorito tool by Rainer Krienke: [Source](https://userpages.uni-koblenz.de/~krienke/ftp/noarch/geteltorito/geteltorito/)
